using StateMachineSystem;
using UnityEngine;

namespace Core
{
    public class Bootstrapper : MonoBehaviour
    {
        private Game _game;
        
        private void Awake()
        {
            Debug.Log("Boostrapper Awake");
            StatesRoot statesRoot = new StatesRoot();
            StateMachine stateMachine = new StateMachine(statesRoot);
            _game = new Game(stateMachine);
        }
    }
}
