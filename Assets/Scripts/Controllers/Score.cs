using System;
using UnityEngine;

namespace Controllers
{
    public class Score
    {
        public event Action<int> OnScoreChanged;
        public event Action<bool> OnActiveStateChanged;

        private int _score;
        

        public int Value
        {
            get => _score;
            set
            {
                _score = value;
                OnScoreChanged?.Invoke(value);
            }
        }
        
        public bool IsActive { get; private set; }

        public void SetActive(bool active)
        {
            IsActive = active;
            OnActiveStateChanged?.Invoke(active);
        }
    }
}
