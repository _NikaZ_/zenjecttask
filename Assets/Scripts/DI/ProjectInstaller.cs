using Controllers;
using Spawn;
using Zenject;

namespace DI
{
    public class ProjectInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            BindScore();
            BindGameOver();
            BindSpawner();
        }

        private void BindSpawner()
        {
            Container.Bind<Spawner>().AsSingle();
        }

        private void BindScore()
        {
            Container.Bind<Score>().AsSingle();
        }

        private void BindGameOver()
        {
            Container.Bind<GameOver>().AsSingle();
        }
    }
}