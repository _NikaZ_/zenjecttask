using System;
using Controllers;
using TMPro;
using UnityEngine;
using Zenject;

namespace Views
{
    public class ScoreView : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _scoreText;
        private Score _score;

        [Inject]
        public void Construct(Score score)
        {
            _score = score;
            _score.OnActiveStateChanged += ChangeActiveState;
        }

        private void ChangeActiveState(bool state)
        {
            gameObject.SetActive(state);
        }

        private void UpdateText(int score)
        {
            _scoreText.text = score.ToString();
        }

        private void OnEnable()
        {
            _score.OnScoreChanged += UpdateText;
            UpdateText(_score.Value);
        }

        private void OnDisable()
        {
            _score.OnScoreChanged -= UpdateText;
        }
    }
}
