using System;
using Controllers;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Views
{
    public class GameOverView : MonoBehaviour
    {
        [SerializeField] private Button _restartButton;
        private GameOver _gameOver;

        [Inject]
        public void Construct(GameOver gameOver)
        {
            _gameOver = gameOver;
        }

        private void OnEnable()
        {
            _restartButton.onClick.AddListener(_gameOver.RestartGame);
        }

        private void OnDisable()
        {
            _restartButton.onClick.RemoveListener(_gameOver.RestartGame);
        }
    }
}
