using System;
using Controllers;
using UnityEngine;
using Zenject;

namespace Spawn
{
    public class Spawner
    {
        public event Action OnSpawnCalled;
        public event Action OnDisableAll;

        public void Spawn()
        {
            OnSpawnCalled?.Invoke();
        }

        public void DisableAll()
        {
            OnDisableAll?.Invoke();
        }
    }
}
