using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Spawn
{
    public class SpawnPoint : MonoBehaviour
    {
        [SerializeField] private GameObject _prefab;
        private List<GameObject> _pool;
        private Spawner _spawner;

        [Inject]
        public void Construct(Spawner spawner)
        {
            _pool = new List<GameObject>();
            _spawner = spawner;
            _spawner.OnSpawnCalled += SpawnObject;
            _spawner.OnDisableAll += DisableObjects;
            Debug.Log("Spawner injected to spawn point");

        }

        private void DisableObjects()
        {
            foreach (GameObject obj in _pool)
            {
                obj.SetActive(false);
            }
        }

        private void OnDestroy()
        {
            _spawner.OnSpawnCalled -= SpawnObject;
            _spawner.OnDisableAll -= DisableObjects;
        }

        private void SpawnObject()
        {
            foreach (var obj in _pool)
            {
                if (!obj.activeSelf)
                {
                    obj.SetActive(true);
                    return;
                }
            }

            GameObject instantiated = Instantiate(_prefab);
            _pool.Add(instantiated);
        }
    }
}
