using System;
using StateMachineSystem.States;
using UnityEngine;

namespace StateMachineSystem
{
    public class StateMachine
    {
        private IState _currentState;
        private StatesRoot _statesRoot;

        public StateMachine(StatesRoot statesRoot)
        {
            _statesRoot = statesRoot;
            SetState(_statesRoot.GetState<InitializeLevelState>());
        }

        public void SetState(IState state)
        {
            _currentState?.Exit();
            _currentState = state;
            _currentState?.Enter();
        }
    }
}
