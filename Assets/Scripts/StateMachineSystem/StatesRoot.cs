using System;
using System.Collections.Generic;
using Controllers;
using Spawn;
using StateMachineSystem.States;
using UnityEngine;
using Zenject;

namespace StateMachineSystem
{
    public class StatesRoot
    {
        private readonly Dictionary<Type, IState> _states;

        public StatesRoot()
        {
            _states = new Dictionary<Type, IState>()
            {
                [typeof(InitializeLevelState)] = new InitializeLevelState(),
                [typeof(GameOverState)] = new GameOverState()
            };
            Debug.Log("States initialized");
        }

        public IState GetState<T>()
        {
            return _states.TryGetValue(typeof(T), out IState state) ? state : null;
        }
    
    }
}
