using Controllers;
using Spawn;
using UnityEngine;
using Zenject;

namespace StateMachineSystem.States
{
    public class InitializeLevelState : IState
    {
        private Spawner _spawner;
        private Score _score;

        [Inject]
        public void Construct(Spawner spawner, Score score)
        {
            _spawner = spawner;
            _score = score;
            Debug.Log("Spawner and score injected to initialize state");
        }

        public void Enter()
        {
            Debug.Log("Initialize state");
            _score.SetActive(true);
            for (int i = 0; i < 100; i++)
            {
                _spawner.Spawn();
            }
        }

        public void Update()
        {
        
        }

        public void Exit()
        {
            _score.SetActive(false);
        }
    }
}
